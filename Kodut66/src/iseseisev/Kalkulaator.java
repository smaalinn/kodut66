package iseseisev;

import lib.TextIO;
import java.util.Scanner;

/**
 * 
 * @version		1.0 10 Dec 2015
 * @author		Sten-Erik Maalinn
 *
 */

public class Kalkulaator {

	public static void main(String[] args) {
		while (true) {
			// Trükib ekraanile nimekirja milliseid teemasid antud programm
			// täidab
			// ning teema ees oleva numbri kirjutamisel liigutakse valitud teema
			// juurde.
			System.out.println("Palun sisestage soovitud teema ees olev number.\n"
					+ "1. Arvuvallad\n2. Liitmine, lahutamine\n3. Korrutamine\n"
					+ "4. Astendamine\n5. Ruutvõrrand.\n6. Arvjadad.\n7. Täisnurkse kolmnurga lahendamine");

			// Küsib teema numbrit.
			int soov = TextIO.getlnInt();
			// Kui sisestatakse soovitud teema, siis liigub programm vastava
			// teema
			// juurde.
			if (soov == 1) {

				// Massiivi kuuluvad numbrid.
				int[] a = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

				// Valib õiged numbrid.
				System.out.println("\nPaarisarvud on: ");
				for (int i = a[0]; i < a.length; i += 2) {
					System.out.print(i + ", ");
				}

				System.out.println("\nPaaritud arvud on: ");
				for (int i = a[1]; i < a.length; i += 2) {
					System.out.print(i + ", ");
				}

				System.out.println("\nNaturaalarvud on: ");
				for (int i = 0; i < a.length; i++) {
					System.out.print(i + ", ");
				}

				System.out.println("\nTäisarvud on: ");
				for (int i = a[0]; i < a.length; i++) {
					System.out.print(i + ", ");

				}
				System.out.println("\n");
			}

			if (soov == 2) {
				// Paneb teema uuesti küsima kui antakse kätte vastus.
				while (true) {
					// Trükib ekraanile juhendi, kuidas programm töötab.
					System.out.println("Liitmine ja lahutamine.\nSisestage arvud ükshaaval.\n"
							+ "Vastuse saamiseks kirjutage viimaseks arvuks 0.\n"
							+ "Lahutamise korral lisage arvu ette miinusmärk.");

					// Küsib järjest numbreid.
					Scanner lugeja = new Scanner(System.in);
					int tulemus = 0;
					while (true) {
						int arv = Integer.parseInt(lugeja.nextLine());
						// Kui sisestatakse 0, siis lõpetab ning trükib
						// ekraanile
						// summa/vahe vastuse.
						if (arv == 0) {
							break;
						}
						tulemus += arv;
					}
					System.out.println("Vastuseks on " + tulemus + ".");
				}
			}

			if (soov == 3) {
				while (true) {
					System.out.println("Korrutamine.\nSisestage arvud ükshaaval.\n"
							+ "Vastuse saamiseks kirjutage viimaseks arvuks 0.\n"
							+ "Negatiivse arvu korral lisage arvu ette miinusmärk.");
					Scanner lugeja = new Scanner(System.in);
					int tulemus = 1;
					while (true) {
						int arv = Integer.parseInt(lugeja.nextLine());
						// Kui sisestatakse 0, siis lõpetab ning trükib
						// ekraanile
						// korrutise vastuse.
						if (arv == 0) {
							break;
						}
						tulemus *= arv;
					}
					System.out.println("Vastuseks on " + tulemus);
				}
			}
			if (soov == 4) {
				while (true) {

					// Küsitakse, millist arvu on vaja astendada ning
					// salvestatkse.
					System.out.println("Astendamine.\nSisesta arv mida on vaja astendada.");
					int arv = TextIO.getlnInt();
					// Küsitakse astendaja.
					System.out.println("Sisesta astendaja.");
					int aste = TextIO.getlnInt();
					// Teostab astendamise ning trükib ekraanile.
					int astenda = (int) Math.pow(arv, aste);
					System.out.println("Vastus on " + astenda + ".");
				}
			}
			if (soov == 5) {
				while (true) {

					// Trükib ekraanile juhendi ning küsib väärtusi.
					System.out.println("Ruutvõrrand.\nVõrrand on kujul ax^2 + bx + c.\n"
							+ "Negatiivse puhul lisage miinusmärk järgneva arvu ette.");
					System.out.println("Sisestage a.");
					int a = TextIO.getlnInt();
					System.out.println("Sisestage b.");
					int b = TextIO.getlnInt();
					System.out.println("Sisestage c.");
					int c = TextIO.getlnInt();
					// Kui sisestatakse väärtused mis täidavad antud tingimuse
					// siis
					// teostatakse tehe ning trükitakse ekraanile. Tingimuse
					// mitte
					// täitmisel trükitakse ekraanile, et lahendused puuduvad.
					if (Math.pow(b, 2) < (4 * a * c)) {
						System.out.println("Vastus: x1 ja x2 puuduvad.");
					} else {
						// Valemi kasutamine vastuste esitamiseks.
						double x1 = (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
						double x2 = (-b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
						System.out.println("Vastus: x1 on " + x1 + " ja x2 on " + x2 + ".");
					}
				}
			}
			if (soov == 6) {
				while (true) {

					// Küsib puuduvad liikmed.
					System.out.println("Sisestage liikme ees olev number, mille väärtust ei tea.");
					System.out.println("1. an ja Sn\n2. an ja d\n3. Sn ja n\n4. a1 ja Sn");
					int leitav = TextIO.getlnInt();
					if (leitav == 1) {

						// Küsib väärtusi tehete tegemiseks.
						System.out.println("Sisestage a1.");
						int a1 = TextIO.getlnInt();
						System.out.println("Sisestage n.");
						int n = TextIO.getlnInt();
						System.out.println("Sisestage d.");
						int d = TextIO.getlnInt();
						// Teostab tehted.
						double an = a1 + (n - 1) * d;
						double Sn = (an - a1) / (n - 1);
						// Trükib välja vastused.
						System.out.println("an = " + an);
						System.out.println("a1 = " + a1);
						System.out.println("n = " + n);
						System.out.println("d = " + d);
						System.out.println("Sn = " + Sn);
					}

					if (leitav == 2) {
						System.out.println("Sisestage a1.");
						int a1 = TextIO.getlnInt();
						System.out.println("Sisestage n.");
						int n = TextIO.getlnInt();
						System.out.println("Sisestage Sn.");
						int Sn = TextIO.getlnInt();
						double an = (2 * n * Sn) - a1;
						double d = (a1 + an) / (n - 1);
						System.out.println("an = " + an);
						System.out.println("a1 = " + a1);
						System.out.println("n = " + n);
						System.out.println("d = " + d);
						System.out.println("Sn = " + Sn);
					}

					if (leitav == 3) {
						System.out.println("Sisestage a1.");
						int a1 = TextIO.getlnInt();
						System.out.println("Sisestage d.");
						int d = TextIO.getlnInt();
						System.out.println("Sisestage an.");
						int an = TextIO.getlnInt();
						double n = ((an - a1) / d) - 1;
						double Sn = (an - a1) / (n - 1);
						System.out.println("an = " + an);
						System.out.println("a1 = " + a1);
						System.out.println("n = " + n);
						System.out.println("d = " + d);
						System.out.println("Sn = " + Sn);
					}

					if (leitav == 4) {
						System.out.println("Sisestage an.");
						int an = TextIO.getlnInt();
						System.out.println("Sisestage n.");
						int n = TextIO.getlnInt();
						System.out.println("Sisestage d.");
						int d = TextIO.getlnInt();
						double a1 = an - ((n - 1) * d);
						double Sn = (an - a1) / (n - 1);
						System.out.println("an = " + an);
						System.out.println("a1 = " + a1);
						System.out.println("n = " + n);
						System.out.println("d = " + d);
						System.out.println("Sn = " + Sn);
					}
				}
			}

			if (soov == 7) {
				while (true) {

					// Trükib ekraanile juhendi ning küsib väärtusi.
					System.out.println("5. Täisnurkse kolmnurga lahendamine.\nSisestage esimene kaatet.");
					int a = TextIO.getlnInt();
					System.out.println("Sisestage teine kaatet.");
					int b = TextIO.getlnInt();
					System.out.println("Kas on teada mingit nurka? Kui on siis 1, kui mitte siis 0.");
					int nurk = TextIO.getlnInt();

					// Kui esitatakse kaks külge.
					if (a > 0 && b > 0 && nurk == 0) {
						double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
						double alfarad = Math.asin(a / c);
						double alfa = 180 / Math.PI * alfarad;
						double beetarad = Math.asin(b / c);
						double beeta = 180 / Math.PI * beetarad;
						double umbermoot = a + b + c;
						double pindala = (a * b) / 2;
						System.out.println("Esimese kaateti pikkus on " + a + ".");
						System.out.println("Teise kaateti pikkus on " + b + ".");
						System.out.println("Hüpotenuusi pikkus on " + c + ".");
						System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
						System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
						System.out.println("Ümbermõõt on " + umbermoot + ".");
						System.out.println("Pindala on " + pindala + ".");
					}

					// Kui esitatakse üks külg.
					if (a > 0 && b == 0 && nurk == 0) {
						System.out.println("Sisestage hüpotenuusi pikkus.");
						// Küsitakse lisaväärtusi, milleks on kolmas külg.
						int c = TextIO.getlnInt();
						// Kui kolmanda külje pikkust ei ole teada, siis vastust
						// ei
						// ole võimalik leida.
						if (c == 0) {
							System.out.println("Lahendus puudub.");
						}
						// Kui esitatakse ka kolmas külg, siis arvutatakse
						// lõpuni.
						if (c > 0) {
							double b1 = Math.sqrt(Math.pow(c, 2) - Math.pow(a, 2));
							double alfarad = Math.asin(a / c);
							double alfa = 180 / Math.PI * alfarad;
							double beetarad = Math.asin(b1 / c);
							double beeta = 180 / Math.PI * beetarad;
							double umbermoot = a + b1 + c;
							double pindala = (a * b1) / 2;
							System.out.println("Esimese kaateti pikkus on " + a + ".");
							System.out.println("Teise kaateti pikkus on " + b1 + ".");
							System.out.println("Hüpotenuusi pikkus on " + c + ".");
							System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
							System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
							System.out.println("Ümbermõõt on " + umbermoot + ".");
							System.out.println("Pindala on " + pindala + ".");
						}
					}
					// Kui esitatakse ainult teine külg.
					if (a == 0 && b > 0 && nurk == 0) {
						// Küsitakse lisaväärtusi.
						int c = TextIO.getlnInt();
						if (c == 0) {
							System.out.println("Lahendus puudub.");
						}
						if (c > 0) {
							double a1 = Math.sqrt(Math.pow(c, 2) - Math.pow(b, 2));
							double alfarad = Math.asin(a1 / c);
							double alfa = 180 / Math.PI * alfarad;
							double beetarad = Math.asin(b / c);
							double beeta = 180 / Math.PI * beetarad;
							double umbermoot = a1 + b + c;
							double pindala = (a1 * b) / 2;
							System.out.println("Esimese kaateti pikkus on " + a1 + ".");
							System.out.println("Teise kaateti pikkus on " + b + ".");
							System.out.println("Hüpotenuusi pikkus on " + c + ".");
							System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
							System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
							System.out.println("Ümbermõõt on " + umbermoot + ".");
							System.out.println("Pindala on " + pindala + ".");
						}
					}
					// Kui esitatakse esimese külje pikkus ning ka teatakse
					// mingit
					// nurka.
					if (a > 0 && b == 0 && nurk == 1) {
						System.out.println("Sisestage nurga alfa suurus kraadides.");
						// Küsitakse alfa nurka. Kui see on 90 kraadi või rohkem
						// ja
						// väiksem kui 0, siis lahendus puudub.
						int alfa = TextIO.getlnInt();
						if (alfa >= 90 || alfa <= 0) {
							System.out.println("Lahendus puudub.");
						}
						// Kui alfa nurka ei teata, siis küsitakse beeta nurga
						// suurust.
						if (alfa == 0) {
							System.out.println("Sisestage nurga beeta suurus kraadides.");
							int beeta = TextIO.getlnInt();
							if (beeta >= 90 || beeta <= 0) {
								System.out.println("Lahendus puudub.");
							}
							if (beeta > 0) {
								int alfa1 = 90 - beeta;
								double beetarad = beeta * Math.PI / 180;
								double c = a / Math.cos(beetarad);
								double b1 = Math.sqrt(Math.pow(c, 2) - Math.pow(a, 2));
								double umbermoot = a + b1 + c;
								double pindala = (a * b1) / 2;
								System.out.println("Esimese kaateti pikkus on " + a + ".");
								System.out.println("Teise kaateti pikkus on " + b1 + ".");
								System.out.println("Hüpotenuusi pikkus on " + c + ".");
								System.out.println("Nurga alfa suurus on " + alfa1 + " kraadi.");
								System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
								System.out.println("Ümbermõõt on " + umbermoot + ".");
								System.out.println("Pindala on " + pindala + ".");
							}
						}
						if (alfa > 0) {
							int beeta = 90 - alfa;
							double alfarad = alfa * Math.PI / 180;
							double c = a / Math.sin(alfarad);
							double b1 = Math.sqrt(Math.pow(c, 2) - Math.pow(a, 2));
							double umbermoot = a + b1 + c;
							double pindala = (a * b1) / 2;
							System.out.println("Esimese kaateti pikkus on " + a + ".");
							System.out.println("Teise kaateti pikkus on " + b1 + ".");
							System.out.println("Hüpotenuusi pikkus on " + c + ".");
							System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
							System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
							System.out.println("Ümbermõõt on " + umbermoot + ".");
							System.out.println("Pindala on " + pindala + ".");
						}
					}
					// Kui esitatakse teise külje pikkus ning teatakse nurka.
					if (a == 0 && b > 0 && nurk == 1) {
						System.out.println("Sisestage nurga alfa suurus.");
						int alfa = TextIO.getlnInt();
						if (alfa >= 90 || alfa <= 0) {
							System.out.println("Lahendus puudub.");
						}
						if (alfa == 0) {
							System.out.println("Sisestage nurga beeta suurus.");
							int beeta = TextIO.getlnInt();
							if (beeta >= 90 || beeta <= 0) {
								System.out.println("Lahendus puudub.");
							}
							if (beeta > 0) {
								int alfa1 = 90 - beeta;
								double beetarad = beeta * Math.PI / 180;
								double c = b / Math.sin(beetarad);
								double a1 = Math.sqrt(Math.pow(c, 2) - Math.pow(b, 2));
								double umbermoot = a1 + b + c;
								double pindala = (a1 * b) / 2;
								System.out.println("Esimese kaateti pikkus on " + a1 + ".");
								System.out.println("Teise kaateti pikkus on " + b + ".");
								System.out.println("Hüpotenuusi pikkus on " + c + ".");
								System.out.println("Nurga alfa suurus on " + alfa1 + " kraadi.");
								System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
								System.out.println("Ümbermõõt on " + umbermoot + ".");
								System.out.println("Pindala on " + pindala + ".");
							}
						}
						if (alfa > 0) {
							int beeta = 90 - alfa;
							double alfarad = alfa * Math.PI / 180;
							double c = b / Math.cos(alfarad);
							double b1 = Math.sqrt(Math.pow(c, 2) - Math.pow(a, 2));
							double umbermoot = a + b1 + c;
							double pindala = (a * b1) / 2;
							System.out.println("Esimese kaateti pikkus on " + a + ".");
							System.out.println("Teise kaateti pikkus on " + b1 + ".");
							System.out.println("Hüpotenuusi pikkus on " + c + ".");
							System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
							System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
							System.out.println("Ümbermõõt on " + umbermoot + ".");
							System.out.println("Pindala on " + pindala + ".");
						}
					}
					// Kui teatakse ainult nurga suurust.
					if (a == 0 && b == 0 && nurk == 1) {
						System.out.println("Sisestage hüpotenuusi pikkus.");
						// Küsitakse lisaväärtusi.
						int c = TextIO.getlnInt();
						if (c == 0) {
							System.out.println("Lahendus puudub.");
						}
						if (c > 0) {
							System.out.println("Sisestage nurga alfa suurus kraadides.");
							int alfa = TextIO.getlnInt();
							if (alfa >= 90 || alfa < 0) {
								System.out.println("Lahendus puudub.");
							}
							if (alfa == 0) {
								System.out.println("Sisestage nurga beeta suurus kraadides.");
								int beeta = TextIO.getlnInt();
								if (beeta >= 90 || beeta <= 0) {
									System.out.println("Lahendus puudub.");
								}
								if (beeta > 0) {
									int alfa1 = 90 - beeta;
									double beetarad = beeta * Math.PI / 180;
									double b1 = c * Math.sin(beetarad);
									double a1 = Math.sqrt(Math.pow(c, 2) - Math.pow(b1, 2));
									double umbermoot = a1 + b1 + c;
									double pindala = (a1 * b1) / 2;
									System.out.println("Esimese kaateti pikkus on " + a1 + ".");
									System.out.println("Teise kaateti pikkus on " + b1 + ".");
									System.out.println("Hüpotenuusi pikkus on " + c + ".");
									System.out.println("Nurga alfa suurus on " + alfa1 + " kraadi.");
									System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
									System.out.println("Ümbermõõt on " + umbermoot + ".");
									System.out.println("Pindala on " + pindala + ".");
								}
							}
							if (alfa > 0) {
								int beeta = 90 - alfa;
								double alfarad = alfa * Math.PI / 180;
								double a1 = c * Math.sin(alfarad);
								double b1 = Math.sqrt(Math.pow(c, 2) - Math.pow(a1, 2));
								double umbermoot = a1 + b1 + c;
								double pindala = (a1 * b1) / 2;
								System.out.println("Esimese kaateti pikkus on " + a1 + ".");
								System.out.println("Teise kaateti pikkus on " + b1 + ".");
								System.out.println("Hüpotenuusi pikkus on " + c + ".");
								System.out.println("Nurga alfa suurus on " + alfa + " kraadi.");
								System.out.println("Nurga beeta suurus on " + beeta + " kraadi.");
								System.out.println("Ümbermõõt on " + umbermoot + ".");
								System.out.println("Pindala on " + pindala + ".");
							}
						}
					}

					// Kui esitatakse muud väärtused, siis vastuseid anda ei ole
					// võimalik.
					if (a == 0 && b == 0 && nurk == 0) {
						System.out.println("Lahendus puudub.");
					}

				}
			}
		}
	}
}