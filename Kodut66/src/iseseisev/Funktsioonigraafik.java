package iseseisev1;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import java.util.function.Function;

/**
 * 
 * @version 1.0 10 Dec 2015
 * @author Sten-Erik Maalinn
 * 
 */

public class Funktsioonigraafik extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	public void start(final Stage tasand) {

		// Telgede mõõtmed.
		telg teljed = new telg(600, 600, -10, 10, 1, -10, 10, 1);
		// Funktsiooni sisu ja asetuse reguleerimine.
		Sisu sisu = new Sisu(x -> 5 * Math.pow(x, 2) - 3 * x - 1, -10, 10, 0.1, teljed);

		// Tekitab kuvandi launcherile.
		StackPane kuva = new StackPane(sisu);
		kuva.setStyle("-fx-background-color: WHITE");
		kuva.setPadding(new Insets(20));

		// Esitab kogu graafiku.
		tasand.setScene(new Scene(kuva));
		tasand.show();
	}

	// Sobitab X-telje aknale.
	private double Xtelg(double x, telg teljed) {
		double k6rguskesktelg = teljed.getPrefWidth()
				/ (teljed.getXAxis().getUpperBound() - teljed.getXAxis().getLowerBound());
		double k6rguskesk = teljed.getPrefWidth() / 2;

		return x * k6rguskesktelg + k6rguskesk;
	}

	// Sobitab Y-telje aknale.
	private double Ytelg(double y, telg teljed) {
		double laiuskesktelg = teljed.getPrefHeight()
				/ (teljed.getYAxis().getUpperBound() - teljed.getYAxis().getLowerBound());
		double laiuskesk = teljed.getPrefHeight() / 2;

		return -y * laiuskesktelg + laiuskesk;

	}

	// Funktsiooni liikumine.
	class Sisu extends Pane {
		// Sisu kirjeldus.
		public Sisu(Function<Double, Double> k, double xMax, double xMin, double samm, telg teljed) {
			Path road = new Path();
			road.setStrokeWidth(3); // Paksus
			road.setStroke(Color.BLACK.deriveColor(0, 1, 1, 1)); // Värvus

			// Graafiku alguse paigutamine.
			road.setClip(new Rectangle(0, 0, teljed.getPrefWidth(), teljed.getPrefHeight()));

			double x = xMax;
			double y = k.apply(x);

			road.getElements().add(new MoveTo(Xtelg(x, teljed), Ytelg(y, teljed)));

			// Punkti haaval liikumine.
			x += samm;
			while (x < xMin) {
				y = k.apply(x);

				road.getElements().add(new LineTo(Xtelg(x, teljed), Ytelg(y, teljed)));

				x += samm;
			}

			// Lisab kõik punktid graafikule.
			setPrefSize(teljed.getPrefWidth(), teljed.getPrefHeight());
			setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
			setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

			getChildren().setAll(teljed, road);
		}

	}

	// Teljestiku paigutamine.
	class telg extends Pane {

		private NumberAxis xTelg;
		private NumberAxis yTelg;

		// Telje defineerimine.
		public telg(int laius, int k6rgus, double xMin, double xMax, double xYhikupikkus, double yMin, double yMax,
				double yYhikupikkus) {
			setPrefSize(laius, k6rgus);
			setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
			setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

			xTelg = new NumberAxis(xMin, xMax, xYhikupikkus);
			yTelg = new NumberAxis(yMin, yMax, yYhikupikkus);
			xTelg.setSide(Side.BOTTOM);
			yTelg.setSide(Side.LEFT);
			xTelg.setMinorTickVisible(false); // Vahepuntid väljas
			yTelg.setMinorTickVisible(false);
			xTelg.setPrefWidth(laius);
			yTelg.setPrefHeight(k6rgus);
			xTelg.setLayoutY(k6rgus / 2);
			yTelg.layoutXProperty().bind(Bindings.subtract((laius / 2) + 1, yTelg.widthProperty()));

			// Paneb teljed antud tingimustel kokku.
			getChildren().setAll(xTelg, yTelg);
		}

		// Tagastab Y-telje
		public NumberAxis getYAxis() {
			return yTelg;
		}

		// Tagasatab X-telje
		public NumberAxis getXAxis() {
			return xTelg;
		}

	}

}